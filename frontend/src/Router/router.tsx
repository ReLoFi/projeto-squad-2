import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'
import * as React from 'react';
import LoginStack from './Login';

import Tabs from './Tabs';
const Stack = createStackNavigator();

function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    name="Tabs"
                    component={Tabs}
                    options={{
                        headerShown:false
                    }}
                />
                
                <Stack.Screen
                    name="Login"
                    component={LoginStack}
                    options={{
                        headerShown:false
                    }}
                />

            </Stack.Navigator>
        </NavigationContainer>

    );
}

export default Router;
