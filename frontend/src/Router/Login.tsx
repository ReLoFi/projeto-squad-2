import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../pages/login';
import Cadastro_1 from '../pages/Cadastro 1';
import Cadastro_2 from '../pages/Cadastro 2';



export default function LoginStack() {
    const Stack = createStackNavigator();

    return (
        <>
            <Stack.Navigator>
                <Stack.Screen
                    name='Login'
                    component={Login}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen
                    name='Cadastro 1'
                    component={Cadastro_1}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen
                    name='Cadastro 2'
                    component={Cadastro_2}
                    options={{
                        headerShown: false
                    }}
                />

            </Stack.Navigator>
        </>
    );
}

