import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Perfil from '../pages/Perfil';
import Postagem from '../pages/Postagem'


export default function PerfilStack() {
    const Stack = createStackNavigator();

    return (
        <>
            <Stack.Navigator>
                <Stack.Screen
                    name='Perfil'
                    component={Perfil}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen
                    name='Postagem'
                    component={Postagem}
                    options={{
                        headerShown: false
                    }}
                />
                

            </Stack.Navigator>
        </>
    );
}

