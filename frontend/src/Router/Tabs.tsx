import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { globalStyles } from '../global/global';
import { AiOutlineHome, AiOutlineShoppingCart, AiOutlineUser } from 'react-icons/ai';
import Carrinho from '../pages/Carrinho';
import ProdutosStack from './Produtos';
import LoginStack from './Login';

 
export default function Tabs() {
    let cor: string;
    const Tab = createBottomTabNavigator(); 

    return (
        <>
            <Tab.Navigator
                screenOptions={{
                    tabBarStyle: {
                        backgroundColor: `${globalStyles.cores.amber}`,
                        height: 65,
                        position: 'absolute',
                         },
                    tabBarShowLabel: false
                    
                }}>
                    <Tab.Screen 
                        name='Home'
                        component={ProdutosStack}
                        options={{
                            tabBarIcon: ({ focused = true}: any) => {
                                cor = focused ? `${globalStyles.cores.gainsboro}` : `black`
                                return <AiOutlineHome style={{fontSize: '40px', color: `${cor}`}}/>
                            },
                            headerShown: false
                        }}
                    />
                    <Tab.Screen 
                        name='Carrinho'
                        component={Carrinho}
                        options={{
                            tabBarIcon: ({ focused = true}: any) => {
                                cor = focused ? `${globalStyles.cores.gainsboro}` : `black`
                                return <AiOutlineShoppingCart style={{fontSize: '40px', color: `${cor}`}}/>
                            },
                            headerShown: false
                        }}
                    />
                    <Tab.Screen 
                        name='Perfil'
                        component={LoginStack}
                        options={{
                            tabBarIcon: ({ focused = true}: any) => {
                                cor = focused ? `${globalStyles.cores.gainsboro}` : `black`
                                return <AiOutlineUser style={{fontSize: '40px', color: `${cor}`}}/>
                            },
                            headerShown: false
                        }}
                    />

            </Tab.Navigator>
        </>
    );
}


