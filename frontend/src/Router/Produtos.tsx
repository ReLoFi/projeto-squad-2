import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../pages/Home';
import Produtos from '../pages/Produtos';
import TelaDeProduto from '../pages/TelaDeProduto';


export default function ProdutosStack() {
    const Stack = createStackNavigator();
    return (
        <>
            <Stack.Navigator>
                <Stack.Screen
                    name='Home'
                    component={Home}
                    options={{
                        headerShown: false
                    }}
                />

                <Stack.Screen
                    name='Produtos'
                    component={Produtos}
                    options={{
                        headerShown: false
                    }}
                    />
                
                <Stack.Screen
                    name= 'TelaDeProduto'
                    component={TelaDeProduto}
                    options={{
                        headerShown: false
                    }}
                    />

            </Stack.Navigator>
        </>
    );
}

