import React from "react";
import { globalStyles } from "../../global/global";
import { AiFillCaretDown } from 'react-icons/ai';
import { View, TextInput, StyleSheet} from "react-native";

export default function InputTexto({param} : any){

    if(param == "Descrição"){
        return(
            <View style={styles.container}>
                <TextInput style={styles.caixatexto2} placeholder={param}/>
            </View>
        )
    }
    else
    if(param == "Categoria" || param == "Condição"){
        return(
            <View style={styles.container}>
                <TextInput style={styles.caixatexto} placeholder={param}/>
                <AiFillCaretDown style={styles.icon}/>
            </View>
        )
    }
    else
    return(
        <View style={styles.container}>
            <TextInput style={styles.caixatexto} placeholder={param}/>
        </View>
    )
}

const styles=StyleSheet.create({

    container:{
        flexDirection: "row",
    },

    caixatexto:{
        backgroundColor:'white',
        borderRadius:10,
        borderWidth: 1,
        border: "solid #E3E3EA",
        marginBottom:22.5,
        marginTop:20,
        fontSize:16,
        fontFamily: `${globalStyles.fontes.m600}`,
        paddingLeft:20,
        color:"black",
        placeholderTextColor:"black 67%",
        width: 387,
        height: 51.38,
        marginLeft: 20,

    },
    caixatexto2:{
        backgroundColor:'white',
        borderRadius:10,
        borderWidth: 1,
        border: "solid #E3E3EA",
        marginBottom:22.5,
        fontSize:16,
        fontFamily: `${globalStyles.fontes.m600}`,
        paddingLeft:20,
        color:"black",
        placeholderTextColor:"black 67%",
        width: 387,
        height: 203,
        marginLeft: 20,

    },
    icon:{
        marginTop:35,
        width: 24,
        height: 24,
        marginLeft:-50,
    }
});