import styled from 'styled-components/native';
import { globalStyles } from '../../global/global';

export const Caixa = styled.TouchableOpacity`
    flex-direction: row;
    justify-content: space-between;
    width: 360px;
    height: 150px;
    background: ${globalStyles.cores.amber};
    border-radius: 16px;
    filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
`;

export const ImagemProduto= styled.Image`
    width: 125px;
    height: 150px;
    border-top-left-radius: 16px;
    border-bottom-left-radius: 16px;
    
`;

export const Informacao = styled.View`
    flex-direction: collum;
    flex: 1;
    justify-content: space-between;
`;

export const CabecaDoCartao = styled.View`
    flex-direction: row;
    flex: 1;
    justify-content: space-between;
    align-items: flex-start;
    margin-left: 8px;
    margin-top: 10px;
`;

export const NomeProduto = styled.Text`
    font-size: 14px;
    color: black;
    font-family: ${globalStyles.fontes.r400};
    width: 123px;
`;


export const Botao = styled.TouchableOpacity`
`;

export const Icones = styled.View`
    flex-direction: row;
    justify-content: center;
    margin-right: 11.57px;
`;

export const PrecoProduto = styled.Text`
    font-size: 14px;
    font-family: ${globalStyles.fontes.r400};
    margin-left: 8px;
    margin-bottom: 24px;
`;
