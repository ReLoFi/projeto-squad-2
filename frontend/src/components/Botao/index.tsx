import React from 'react';
import { globalStyles } from "../../../src/global/global";
import { Text, View, StyleSheet } from "react-native";

type BotaoInfo = {
    value: string;
 
}


const Botao = ({ value } : BotaoInfo) =>  {

    if(value == "Editar Perfil"){
        return(
           
    
            <View style={styles.btn}>
                <Text style={styles.txt2}> {value} </Text>
            </View>
            
    
        )
    }
    if(value == "Confirmar"){
        return(
    
            <View style={styles.btn2}>
                <Text style={styles.txt}> {value} </Text>
            </View>
           
    
        )
    }
    else
    if(value == "Salvar rascunho" || value == "Editar Perfil"){
        return(
    
            <View style={styles.btn}>
                <Text style={styles.txt}> {value} </Text>
            </View>
           
    
        )
    }
    return(

        <View style={styles.btn}>
            <Text style={styles.txt2}> {value} </Text>
        </View>
       

    )

}
const styles=StyleSheet.create({

    btn:{

        width: 167,
        height:52,
        backgroundColor:"#FFBD00",
        borderRadius:10,
        alignItems:"center",
        justifyContent:"center",
        flexDirection:"row",

    },
    btn2:{
        width:335,
        height:52,
        borderRadius: 10,
        backgroundColor:"#FFBD00",
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center",

    },

    txt:{
        fontFamily: globalStyles.fontes.mB700,
        fontSize:14,
        color:"black",
        textAlign:"center",

    },
    txt2:{
        fontFamily: globalStyles.fontes.m500,
        fontSize:20,
        color:"black",
        textAlign:"center",

    }
})
export default Botao;