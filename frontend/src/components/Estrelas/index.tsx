import * as React from 'react';
import { AiFillStar, AiOutlineStar } from 'react-icons/ai';
import { globalStyles } from '../../global/global';
import { Estrelas, Linha, Texto } from './style';


type QuantideEstrelas = {
    QuantideCheias: number;
    QuantideVazias: number;
    Media : number;
}



export default function Comentario( {QuantideCheias, QuantideVazias,Media } : QuantideEstrelas) { 

    var stars = [];

    /* Quantidade de estrelas da avaliação */
    for (let i = 0; i < QuantideCheias ; i++){
        stars.push(<AiFillStar style={{color: `${globalStyles.cores.amber}`, fontSize: 24}} />)
    }
    for (let i = 0; i < QuantideVazias ; i++){
        stars.push(<AiOutlineStar style={{ fontSize: 24}} />)
    }
       
    return (
        <Linha>
            {/* Quantidade de estrelas da avaliação */}
            <Estrelas>
                {
                    stars.map((star) =>
                        star
                    )
                }
            </Estrelas>
            {/* Media de estrelas da avaliação */}
            <Texto> {Media} de 5</Texto>
        </Linha>
    );
}