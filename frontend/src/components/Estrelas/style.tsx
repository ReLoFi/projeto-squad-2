import styled from 'styled-components/native';
import { globalStyles } from '../../global/global';

export const Linha = styled.View`
    flex-direction: row;
    align-items: center;
    gap: 12px;
    margin-left: 15.07px;
    margin-top: 7.06px;
    margin-bottom: 37.04px;
`;

export const Estrelas = styled.View`
    flex-direction: row;
    align-items: center;
    gap: 6.14px;
`; 


export const Texto = styled.Text`
    font-family: ${globalStyles.fontes.r400};
    font-size: 20px;
    color: black;
`;
