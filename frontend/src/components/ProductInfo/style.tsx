import styled from 'styled-components/native';
import { globalStyles } from '../../global/global';

export const Fundo = styled.View`
    width: 100%;
    background-color: ${globalStyles.color.gray};
    font-family: ${globalStyles.fonts.r400};
    margin-top: 27px;
    border-radius: 10px;
    gap: 16px;
    padding-left: 13px;
    padding-top: 16px;
    padding-bottom: 41px;
    flex-direction: column;
`;

export const Name = styled.Text`
    font-size: 24px;
    color: black;
`;

export const Title = styled.Text`
    font-size: 36px;
    color: black;
`;

export const Preco = styled.Text`
    font-size: 24px;
    color: black;
`;

export const TextDesc = styled.Text`
    font-size: 16px;
    color: black;
`;

export const BtnComprar = styled.TouchableOpacity`
    background-color: ${globalStyles.color.Buckthorn};
    align-items: center;
    justify-content: center;
    width: 385px;
    height: 40px;
    border-radius: 16px;
    margin-bottom: 16px;
`;

export const BtnCarrinho = styled.TouchableOpacity`
    background-color: ${globalStyles.color.amber};
    align-items: center;
    justify-content: center;
    width: 385px;
    height: 40px;
    border-radius: 16px;
`;

export const TextBtn = styled.Text`
    font-size: 20px;
    color: black;
    
`;
