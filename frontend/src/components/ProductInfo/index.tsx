import * as React from 'react';
import { View } from 'react-native';
import { BtnCarrinho, BtnComprar, Fundo, Name, Preco, TextBtn, TextDesc, Title } from './style';

type ProductData = {
    name: string;
    Price: number;
    Descpription: string;
}


export default function ProductInfo({name, Price, Descpription} : ProductData) { 
       
    return (
        <Fundo>
             {/* Nome do produto */}
            <Name>{name}</Name>
             {/* Preço*/}
            <Title>Preço</Title>
            <View> 
                <Preco>
                    R$ {Price.toFixed(2)}
                </Preco>
                <Preco>
                    10x de  R$ {Number(Price / 10).toFixed(2)}
                </Preco>
            </View>
            {/* Descrição */}
            <Title>Descrição</Title>
            <TextDesc>
            {Descpription}
            </TextDesc>
           <View style={{alignSelf:'center'}}>
            {/* Botões */}
            <BtnComprar>
                <TextBtn>
                    Comprar agora
                </TextBtn>
            </BtnComprar>
            
            <BtnCarrinho>
                <TextBtn>
                    Adicionar ao carrinho
                </TextBtn>
            </BtnCarrinho>
           </View>

        </Fundo>
    );
}