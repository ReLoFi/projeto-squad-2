import styled from 'styled-components/native';
import { globalStyles } from '../../global/global';


export const FundoCaixa = styled.TouchableOpacity`
    width: 100px;
    height: 90px;
    background: ${globalStyles.cores.amber};
    border-radius: 16px;
    flex-direction: collum;
    justify-content: center;
    align-items: center;
    margin-right: 42px;
`;

export const Texto = styled.Text`
    font-size: 16px;
    font-family: ${globalStyles.fontes.r400};
    color: black;
    text-align: center;
`;
