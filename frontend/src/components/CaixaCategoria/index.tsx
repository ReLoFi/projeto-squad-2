import React from 'react';
import { FundoCaixa, Texto } from './style';


type Nome = {
    nome: string;
}

export function CaixaCategoria({nome}: Nome){

    return(
        <FundoCaixa><Texto>{nome}</Texto></FundoCaixa>
    )
}
