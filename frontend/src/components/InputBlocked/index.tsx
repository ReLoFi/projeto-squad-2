import React from "react";
import { globalStyles } from "../../global/global";
import { View, TextInput, StyleSheet} from "react-native";

export default function InputTexto({param} : any){

    return(
        <View style={styles.container}>
            <TextInput style={styles.caixatexto} placeholder={param}  editable={false}/>
        </View>
    )
}

const styles=StyleSheet.create({

    container:{
        flexDirection: "row",
    },

    caixatexto:{
        backgroundColor:'white',
        borderRadius:10,
        borderWidth: 1,
        border: "solid #E3E3EA",
        marginBottom:22.5,
        marginTop:20,
        fontSize:16,
        fontFamily: `${globalStyles.fontes.m600}` ,
        paddingLeft:20,
        color:"black",
        placeholderTextColor:"black",
        width: 387,
        height: 51.38,
        marginLeft: 20,

    }
});