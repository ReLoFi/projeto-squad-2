import React from 'react';
import { GoSearch } from 'react-icons/go';
import { BarraBusca, Botao, Busca  } from './style';
import { useNavigation } from "@react-navigation/native";
import { useHandleSearch } from '../../contexts/search';


export function BuscaBarra() {

    const navigation = useNavigation();

    const search = useHandleSearch();

    return(
        <BarraBusca>
            <Busca placeholder="Buscar Produto" onChange={(event: any) => search(event)}></Busca>
            <Botao onPress={() => navigation.navigate('Produtos' as never) } >
                <GoSearch style={{ color: 'black', fontSize: "30px", marginTop: "7.83px", marginRight: "18.83px" }} />
            </Botao>
        </BarraBusca>
    )
}
