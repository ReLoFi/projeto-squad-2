import styled from 'styled-components/native';
import { globalStyles } from '../../global/global';

export const BarraBusca = styled.View`
    width: 87.35%;
    height: 41px;
    background: white;
    border-radius: 16px;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    align-self: center;

`;

export const Busca = styled.TextInput`
    width: 100%;
    height: 100%;
    font-size: 16px;
    font-family: ${globalStyles.fontes.m500};
    margin: 22px 11px;
    color: rgba(0, 0, 0, 0.5);
`;

export const Botao = styled.TouchableOpacity`
    
`;