import React, {useState} from "react";
import {StyleSheet,Text} from "react-native";
import {useForm,Controller} from 'react-hook-form'
import {InputSection,TextInputt,TextInputSection} from "./style";
import { TextInputMask } from 'react-native-masked-text';

interface  FormData {
    nome: string,
    email: string,
    cpf: string,
    senha: string,
    data: string,
}



// inputs text da tela cadastro 1
export function TextInput() {
    const [cpf,setCpf] = useState('');
    const { control, handleSubmit, formState:{errors}, getValues } = useForm<FormData>({mode: "onTouched"});
    return(
        <InputSection>
            
            <TextInputSection>
            <TextInputt 
                placeholder="Nome Completo"
                type={'name'}
                
            />
            </TextInputSection>
            
            <TextInputSection>
                <Controller
                control={control}
                    name='cpf'
                    defaultValue=''
                    render={({field:{onBlur, onChange, value}}) => (
                        <TextInputMask 
                            style={styles.input}
                            type={'cpf'}
                            onBlur={onBlur}
                            placeholder='CPF'
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                        />
                        )}
                        rules={{
                                    required: "CPF é obrigatório",
                                    pattern: {
                                        value: /^[a-zA-Z0-9]+$/i,
                                        message: 'Somente números permitidos'
                                    },
                                }}
                />
                    {errors.cpf && <Text>{errors.cpf.message}</Text>}

            </TextInputSection>
            
            <TextInputSection>
                <Controller
                    control={control}
                        name='data'
                        defaultValue=''
                        render={({field:{onBlur, onChange, value}}) => (
                        
                        <TextInputMask 
                        style={styles.input}
                        type={'datetime'}
                        onBlur={onBlur}
                        onChangeText={(value:any) => onChange(value)}
                        options={{
                            format:'DD/MM/YYYY'
                        }}
                        placeholder="Data de nascimento"
                        value={value}
                        />
                        )}
                        rules={{
                                    required: "Data de é obrigatório",
                                    pattern: {
                                        value: /^[a-zA-Z0-9]+$/i,
                                        message: 'Somente números permitidos'
                                    },
                                }}
                    />
                    {errors.data && <Text>{errors.data.message}</Text>}
                
            </TextInputSection>
    
            <TextInputSection>
                
                <Controller
                        control={control}
                        name='email'
                        defaultValue=''
                        render={({field:{onBlur, onChange, value}}) => (
                            <TextInputMask
                                type={'custom'}
                                options={{
                                    mask: '**********'
                                }}
                                style={styles.input}
                                onBlur={onBlur}
                                onChangeText={(value:any) => onChange(value)}
                                value={value}
                                placeholder='Email'
                                
                            />
                        )}
                        rules={{
                            required: "Email é obrigatório",
                            pattern: {
                                value: /^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                message: "Formato de email inválido"
                            },
                        }}
                    />
                    {errors.email && <Text>{errors.email.message}</Text>}

            </TextInputSection>
            <TextInputSection>
                <TextInputt 
                secureTextEntry={true}
                placeholder="Senha"
                    type={''}
                />
            </TextInputSection>
        </InputSection>
    )
}
const styles= StyleSheet.create ({
    input: {
        width:387,
        height:51.5,
        borderRadius: 15,
        borderWidth:1,
        borderColor: '#000000c7',
        backgroundColor: '#FFFFFF',
        paddingLeft:7,
        border:0

    }
})
export default TextInput;