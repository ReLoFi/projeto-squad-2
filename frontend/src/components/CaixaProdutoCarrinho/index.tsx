import React, { useState } from 'react';
import { Caixa, Botao, Informacao, ImagemProduto, CabecaDoCartao, NomeProduto, Icones, PrecoProduto } from './style';
import { AiFillHeart, AiOutlineDelete, AiOutlineHeart} from 'react-icons/ai'
import { useNavigation } from '@react-navigation/native';


type infoProduto = {
    nome: string;
    preco: number;
    nomeImagem: string;
}


export function CaixaProdutoCarrinho({nome, preco, nomeImagem} : infoProduto) {
    
    const navigation = useNavigation();

    const [Salvo, setSalvo ]= useState(false);
    let CorCoracao : string;
    let CorCoracaoContorno : string;
    
    if (Salvo) {
        CorCoracao = 'red';
        CorCoracaoContorno = 'black';
    }else{
        CorCoracao = `transparent`;
        CorCoracaoContorno = 'black';
    }
    


    return(
        <Caixa onPress={() => navigation.navigate('TelaDeProduto' as never) } > 
            <ImagemProduto source={require(`../../../assets/${nomeImagem}.jpg`)}/>
            <Informacao> 
                <CabecaDoCartao> 
                    <NomeProduto>{nome}</NomeProduto>

                    <Icones>
                        <Botao onPress={() => setSalvo(!Salvo)}>
                            <AiFillHeart style={{fontSize: '40px', color: `${CorCoracao}` }}/>
                            <AiOutlineHeart style={{position:'fixed', fontSize: '40px', color: `${CorCoracaoContorno}` }}/>
                        </Botao>
                        <Botao  onPress={() => alert('Produto adicionado no carrinho')}> 
                            <AiOutlineDelete style={{fontSize: '40px' }} />
                        </Botao>
                    </Icones>
                </CabecaDoCartao>
                <PrecoProduto>R$ {preco}</PrecoProduto>
            </Informacao>
        </Caixa>

    )
}
