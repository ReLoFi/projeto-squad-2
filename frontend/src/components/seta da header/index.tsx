import React from 'react';
import {Image, View,TouchableOpacity, StyleSheet, Text } from "react-native";
import {IconSection} from "./style"
import Icon from 'react-native-vector-icons/AntDesign'
import {useNavigation} from '@react-navigation/native';


// Input da seta da header
export function Input (){
    const navigation = useNavigation();
    return(
        
        <IconSection>
            <Icon
                onPress={()=>navigation.goBack()}
                style={style.icon} name="arrowleft" size={40}>
            
            </Icon>
        </IconSection>

    )
}
const style= StyleSheet.create({
    icon:{
        color:'#FFBD00'
    }
})
export default Input;