import * as React from 'react';
import { View } from 'react-native';
import { BotaoCarrinho, BotaoComprar, Descricao, Fundo, Nome, Preco, TextoBotao, Titulo } from './style';

type DadosProduto = {
    nome: string;
    preco: number;
    descricao: string;
}


export default function InfoProduto({nome, preco, descricao} : DadosProduto) { 
       
    return (
        <Fundo>
             {/* Nome do produto */}
            <Nome>{nome}</Nome>
             {/* Preço*/}
            <Titulo>Preço</Titulo>
            <View> 
                <Preco>
                    R$ {preco.toFixed(2)}
                </Preco>
                <Preco>
                    10x de  R$ {Number(preco / 10).toFixed(2)}
                </Preco>
            </View>
            {/* Descrição */}
            <Titulo>Descrição</Titulo>
            <Descricao>
            {descricao}
            </Descricao>
           <View style={{alignSelf:'center'}}>
            {/* Botões */}
            <BotaoComprar>
                <TextoBotao>
                    Comprar agora
                </TextoBotao>
            </BotaoComprar>
            
            <BotaoCarrinho>
                <TextoBotao>
                    Adicionar ao carrinho
                </TextoBotao>
            </BotaoCarrinho>
           </View>

        </Fundo>
    );
}