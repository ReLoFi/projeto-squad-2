import * as React from 'react';
import { AiFillStar, AiOutlineStar } from 'react-icons/ai';
import { View } from 'react-native';
import { globalStyles } from '../../global/global';
import { Botoes, Botao, TextoBotao, TextoComentario, Estrelas, Fundo, Perfil, FotoPerfil, NomePerfil } from './style';

type ComentarioData = {
    Nome: string;
    FotoUrl: string;
    QuantideCheias: number;
    QuantideVazias: number;
    Comentario : string;

}



export default function Comentario({Nome, FotoUrl ,QuantideCheias, QuantideVazias,Comentario } : ComentarioData) { 
       
    /* Faz as estrelas da avaliação */
    var stars = [];

    for (let i = 0; i < QuantideCheias ; i++){
        stars.push(<AiFillStar style={{color: `${globalStyles.cores.amber}`, fontSize: 18}} />)
    }
    for (let i = 0; i < QuantideVazias ; i++){
        stars.push(<AiOutlineStar style={{ fontSize: 18}} />)
    }

    return (
        <Fundo>
            {/* Foto e nome do usuario */}
            <Perfil>
                <FotoPerfil source={require(`../../../assets/${FotoUrl}`)}/>
                <View style={{flexDirection:'column',alignItems:'center'}}>
                    <NomePerfil>{Nome}</NomePerfil>
                    <Estrelas>
                    {
                        stars.map((star) =>
                            star
                        )
                    }
                    </Estrelas>
                </View>

            </Perfil>

            {/* Corpo do comentario */}
            <TextoComentario>
            {Comentario}
            </TextoComentario>

            {/* Botão do comentario */}
            <Botoes>
                <Botao>
                    <TextoBotao>
                        Editar Comentario
                    </TextoBotao>
                </Botao>

                <Botao>
                    <TextoBotao>
                        Deletar Comentario
                    </TextoBotao>
                </Botao>
            </Botoes>


        </Fundo>
    );
}