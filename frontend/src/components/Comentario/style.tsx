import styled from 'styled-components/native';
import { globalStyles } from '../../global/global';

export const Fundo = styled.View`
    background-color: ${globalStyles.cores.gainsboro};
    font-family: ${globalStyles.fontes.r400};
    width: 93.4%;
    align-self: center;
    margin-bottom: 7px;
    border-radius: 5px;
`;

export const Perfil = styled.View`
    flex-direction: row;
    align-items: center;
    gap: 8px;
`

export const FotoPerfil = styled.Image`
    width: 14.7%;
    aspect-ratio: 1.1;
    margin-left: 7px;
    margin-top: 7px;
`;

export const NomePerfil = styled.Text`
    font-family: ${globalStyles.fontes.e700};
`;

export const Estrelas = styled.View`
    flex-direction: row;
    align-items: center;
    gap: 5.12px;
`; 

export const TextoComentario = styled.Text`
    font-size: 16px;
    margin-top: 16px;
    margin-left: 10px;
    width: 365px;
`;

export const Botoes = styled.View`
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
    flex-wrap: wrap;
    margin-top: 25px;
    margin-bottom: 5px;
    margin-right: 15px;
    gap: 14px;
`;

export const Botao = styled.TouchableOpacity`
`;

export const TextoBotao = styled.Text`
    font-size: 14px;
`;

