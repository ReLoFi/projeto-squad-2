import React, {useState, useContext, createContext, useEffect} from "react";

const Logado = createContext("");

const HandleLogin = createContext((event : any) => {});

export const Logar = ({ children }: any) => {
    const [isLogado, setLogado] = useState("")

    const handleLogin = (event : any) => {
        setLogado(event.target.value)
    }

    useEffect(() => {
        console.log(isLogado);
    }, [isLogado])


    return (

        <Logado.Provider value={isLogado}>
            <HandleLogin.Provider value={handleLogin}>
                {children}
            </HandleLogin.Provider>
        </Logado.Provider>

    )
}

export const useLogado = () => {
    const logado = useContext(Logado);

    return logado;
}

export const useHandleLogin = () => {
    const handleLogin = useContext(HandleLogin);

    return handleLogin;
}