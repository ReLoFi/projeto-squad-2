export const produtos = [
    {
        nome: 'Violão Elétrico Aço Roadster Parlor TWR2 P E Natural Tanglewood',
        preco: 2479.00,
        nomeImagem: 'Violao',
        descricao: `QUE É UM VIOLÃO PARLOR?
        Violões Parlor possuem dimensões físicas menores se comparados aos violões mais populares e se destacam bastante nos palcos e na parede de nossa loja diantes dos instrumentos mais convencionais.`,
    },
    {
        nome: 'Bateria Infantil BIP-14 PHX - Preto (BK)',
        preco: 849.00,
        nomeImagem: 'BateriaInfantil',
        descricao: `BATERIA INFANTIL PHX
        BIP-14
        A bateria infantil BIP-14 da PHX é um instrumento para iniciantes na musica, essa linha foi criada para trazer o rico universo da música e oferecer instrumentos diferenciados, incríveis e de qualidade.
        Os produtos PHX se destacam pelo acabamento, sonoridade e principalmente pela inovação em toda linha. Os desafios vão além do desenvolvimento de novos produtos, pois está sempre focada no rigoroso controle de qualidade`,
    },
    {
        nome: 'Encordoamento para Guitarra 010 Nickel Nanoweb Elixir',
        preco: 189.00,
        nomeImagem: 'Encordamento',
        descricao: `A Corda Feita para Durar
        Os encordoamentos Elixir possuem em fino revestimento que protege toda a corda contra corrosão e o acumulo de sujeira, que podem comprometer o timbre ao longo do tempo. As cordas Elixir têm a maior duração do timbre comparado as outras marcas de cordas, revestidas ou não revestidas. Têm um som ótimo após vários shows. Sessão após sessão.
        `,
    },
    {
        nome: 'Banco Para Piano e Teclado Casio CB-30BK Ajustável Preto Casio',
        preco: 949.00,
        nomeImagem: 'BancoPiano',
        descricao: `Qualidade e Versatilidade
        O Banco Banqueta Casio Piano CB-30 BK Preta é uma das melhores opções para os músicos e pianistas.
        O Banco da Casio é todo preto, com excelente acabamento e regulagem de altura. Indicado para Piano Digital, Piano Acústico, Teclado, Sintetizador e até mesmo para outros Instrumentos.`,
    },
    
]