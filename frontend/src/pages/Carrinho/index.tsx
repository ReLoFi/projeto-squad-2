import { useNavigation } from '@react-navigation/native';
import * as React from 'react';
import { AiOutlineArrowLeft } from 'react-icons/ai';
import { ScrollView } from 'react-native';
import { CaixaProdutoCarrinho } from '../../components/CaixaProdutoCarrinho';
import { produtos } from '../../constants/produtos';
import { globalStyles } from '../../global/global';
import { BotaoComprar, BotaVoltar, Cabeca, CarrinhoInfo, Fundo, Info, InfoNome, ListaProdutos, TextoBotao, Titulo, Valor } from './style';




export default function Carrinho() {
    const navigation = useNavigation();     

    /*Preço do carrinho */
    var ValorInicial = 0; 
    var PrecoProdutos = produtos.reduce(function (acumulador, valorAtual) {
    return acumulador + valorAtual.preco;
}, ValorInicial)
    var Frete = 10;
    var PrecoTotal = Frete + PrecoProdutos;

  
    return (
        <Fundo> 
            {/* Cabeçalho do carrinho */}
            <Cabeca>
                {/*Botão de voltal */}
                <BotaVoltar onPress={() => navigation.goBack() }>
                    <AiOutlineArrowLeft style={{ fontSize: '40px' ,color: `${globalStyles.cores.amber}` }}/>
                </BotaVoltar> 

                <Titulo>Carrinho</Titulo>
            </Cabeca>

        
            <ScrollView style={{ flex: 1 }}> 
                {/*Lista de produtos*/}
                <ListaProdutos> 
                {
                    produtos.map((produto) => 
                    <CaixaProdutoCarrinho
                        nome = {produto.nome}
                        preco = {produto.preco}
                        nomeImagem = {produto.nomeImagem}
                    />
                    )
                }              
                </ListaProdutos>
            </ScrollView>

            {/*Informção do carrinho */}    
            <CarrinhoInfo> 
                <Info>
                    <InfoNome>Preço Produtos:</InfoNome>
                    <Valor>R$ {PrecoProdutos}</Valor>
                </Info>
                <Info>
                    <InfoNome>Preço Frete:</InfoNome>
                    <Valor>R$ {Frete}</Valor>
                </Info>
                <Info>
                    <InfoNome>Preço Total:</InfoNome>
                    <Valor>R$ {PrecoTotal}</Valor>
                </Info>
                {/*Botao de finalizar compra */}
                <BotaoComprar>
                    <TextoBotao>
                        Finalizar Compra
                    </TextoBotao>
                </BotaoComprar>

            </CarrinhoInfo>

        </Fundo>
    );
  }

