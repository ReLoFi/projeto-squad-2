import styled from 'styled-components/native';
import { globalStyles } from '../../global/global';

export const Fundo = styled.View`
    background: ${globalStyles.cores.jet};
    flex: 1;
`;

export const Cabeca = styled.View`
    flex-direction: row;
    align-items: center;
    flex-wrap: wrap;
    margin-left: 31.62px;
    margin-top: 34.09px;
    gap: 19.62px;
`;

export const BotaVoltar = styled.TouchableOpacity`
    
`;

export const Titulo = styled.Text`
    font-family: ${globalStyles.fontes.r400};
    color: white;
    font-size: 24px;
`;

export const ListaProdutos = styled.View`
    padding-top: 20px;
    padding-bottom: 20px;
    gap: 20px;
    flex-direction: collum;
    justify-content: flex-start;
    align-items: center;
`;

export const CarrinhoInfo = styled.View`
    width: 100%;
    height: 27%;
    border-top-right-radius: 25px;
    border-top-left-radius: 25px;
    padding-top: 23px;
    background: ${globalStyles.cores.gainsboro};
    flex-direction: column;

`;

export const Info = styled.View`
    flex-direction: row;
    justify-content: space-between;
    margin-bottom: 11px;
    flex-wrap: wrap;
`;

export const InfoNome = styled.Text`
    font-size: 20px;
    font-family: ${globalStyles.fontes.r400};
    color: black;
    margin-left: 17px;
`;

export const Valor = styled.Text`
    font-size: 20px;
    font-family: ${globalStyles.fontes.r400};
    color: black;
    margin-right: 17px;
`;

export const BotaoComprar = styled.TouchableOpacity`
    width: 175px;
    height: 40px;
    background-color: ${globalStyles.cores.amber};
    border-radius: 16px;
    justify-content: center;
    align-items: center;
    align-self: flex-end;
`;

export const TextoBotao = styled.Text`
    font-size: 20px;
    font-family: ${globalStyles.fontes.r400};
    color: black;
`;

