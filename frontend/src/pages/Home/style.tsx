import styled from 'styled-components/native';
import { globalStyles } from '../../global/global';

export const Fundo1 = styled.View`
    background: ${globalStyles.cores.jet};
    flex: 1;
    height: 100%;
    width: 100%;
`;

export const Botao = styled.TouchableOpacity`

`;

export const Logo = styled.Image`
    width: 290px;
    height: 56px;
    align-self: center;
    margin-top: 55px;
    margin-bottom: 11px;
`;

export const Titulo = styled.Text`
    font-family: ${globalStyles.fontes.m400};
    color: white;
    font-size: 32px;
    margin: 17px;
`;

export const ListaCategoria = styled.ScrollView`
    padding-left: 21px;
    max-height: 90px;
`;

export const BotaoMaisProdutos = styled.TouchableOpacity`
    background-color: ${globalStyles.cores.amber};
    width: 190px;
    height: 50px;
    justify-content: center;
    align-items: center;
    border-radius: 16px;
`;

export const BotaoText = styled.Text`
    font-family: ${globalStyles.fontes.r400};
    font-size: 20px;
`;

export const Fundo2 = styled.View`
    flex: 1;
    width: 100%;
    height: 55%;
    border-top-right-radius: 25px;
    border-top-left-radius: 25px;
    background: ${globalStyles.cores.gainsboro};
`;

export const ListaProdutos = styled.View`
    padding-top: 20px;
    padding-bottom: 85px;
    gap: 20px;
    flex-direction: collum;
    justify-content: flex-start;
    align-items: center;
`;




