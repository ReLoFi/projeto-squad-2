import * as React from 'react';
import { AiOutlineSetting } from 'react-icons/ai';
import { ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useSearchValue } from '../../contexts/search';
import { Botao, BotaoMaisProdutos, BotaoText, Fundo1, Fundo2, ListaCategoria, ListaProdutos, Logo, Titulo } from './style';
import { CaixaCategoria } from '../../components/CaixaCategoria';
import { categorias } from '../../constants/categorias';
import { produtos } from '../../constants/produtos';
import { CaixaProduto } from '../../components/CaixaProduto';
import { BuscaBarra } from '../../components/BuscaBarra';


function Home() {
  const navigation = useNavigation();
  const searchValue = useSearchValue(); 
     
  return (
    <Fundo1> 

      {/* Botão de compra */}  
      <Botao>

        <AiOutlineSetting style={{position: 'absolute' ,fontSize: '35px', color: `rgba(255, 189, 0, 0.62)`,marginTop: '29px', paddingRight: '22px', alignSelf: 'flex-end' }}/>      

      </Botao>

      {/*Logo */}
      <Logo source={require('../../../assets/EJCMusic.png')} />  
      

      <BuscaBarra/>

      <Titulo>Categorias</Titulo>

      {/* Lista da categorias */}
      <ListaCategoria  horizontal={true} removeClippedSubviews={true} >
          {
              categorias.map((categoria) => 
              <CaixaCategoria nome={categoria.nome}/>
              )
          }
      </ListaCategoria>

      <Titulo>Destaques</Titulo>
      
      {/*Fundo dos produtos destaques */}
      <Fundo2>
        <ScrollView style={{height:'55.9%' }}>
        {/*Lista com produtos */} 
        <ListaProdutos>
            {
              produtos.map((produtos) => {
                if(produtos.nome.toLowerCase().includes(searchValue.toLowerCase())){
                  return (
                    <CaixaProduto 
                      nome = {produtos.nome}
                      preco = {produtos.preco}
                      nomeImagem = {produtos.nomeImagem}
                    />
                  )
                } 
              }
            )}
            {/* Botão de produtos */}
            <BotaoMaisProdutos  onPress={() => navigation.navigate('Produtos' as never) }>
              <BotaoText>Ver mais produtos</BotaoText>
            </BotaoMaisProdutos>
          </ListaProdutos>
        </ScrollView>
      </Fundo2>
    </Fundo1>

  );
}


export default Home;
