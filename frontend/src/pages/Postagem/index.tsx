import * as React from 'react';
import { AiOutlineArrowLeft, AiOutlineUser } from 'react-icons/ai';
import {FundoApp, UpperBox, TxtHeader, ImageHolder, LowerBox, BtnWrapper, TxtLabel,Txt1, Txt2,Txt3, InfoWrapper, Info, Cloud, Perfil, Txt4} from '../Postagem/style';
import Botao from '../../components/Botao';
import InputTexto from '../../components/InputTexto';
import { useNavigation } from '@react-navigation/native';
import { ScrollView, TouchableOpacity } from 'react-native';

function Postagem() {
    const navigation = useNavigation(); 

    return (
        <FundoApp>
            {/* Cabeçalho da página */}
            <TouchableOpacity  onPress={() => navigation.goBack()}>
                <InfoWrapper>
                    <AiOutlineArrowLeft style={styles.arrow}/>
                    <TxtHeader> Anunciar </TxtHeader>
                </InfoWrapper>
            </TouchableOpacity>

            {/* Informações do conteúdo da página */}
            <InfoWrapper>
                <Info>
                    <Txt1> Marketplace</Txt1>
                    <Txt2>Item para venda</Txt2>
                </Info>
                <Botao value='Salvar rascunho'/>
                
            </InfoWrapper>
            
            <ScrollView contentContainerStyle={{paddingBottom: 800}} >
                {/* Área de upload das imagens do produto */}
                <UpperBox>
                    {/* Perfil que está fazendo o anuncio */}
                    <InfoWrapper>
                        <Perfil>
                                <AiOutlineUser style={styles.userIcon}/>
                        </Perfil>
                        <Txt4> Ciclano da Silva</Txt4>
                    </InfoWrapper>

                    {/* Fotos do produto */}
                    <TxtLabel> Fotos 0/10 - Você pode adicionar até 10 fotos </TxtLabel>   
                    <ImageHolder>
                        <Cloud source={require('../../../assets/cloud.png')}/>
                        <Txt3> Adicionar fotos </Txt3>
                    </ImageHolder>

                </UpperBox>

                {/* Área de informações do produto */}
                <LowerBox>
                        <InputTexto param={"Título"}/>
                        <InputTexto param={"Preço"}/>
                        <InputTexto param={"Categoria"}/>
                        <InputTexto param={"Condição"}/>
                        <InputTexto param={"Descrição"}/>
                </LowerBox>


                {/* Botão de confirmação */}
                <BtnWrapper>
                    <Botao value='Confirmar'/>
                </BtnWrapper>

            </ScrollView>


        </FundoApp>
    )}

    // Estilização dos icones
    const styles = ({
        userIcon: {
            height: 35,
            width: 34,
            marginLeft:13,
            marginTop: 10,
            color:'black',
        },
        arrow:{
            height: 40,
            width: 40,
            marginLeft:29,
            paddingTop:40,
            color:'#FFBD00',
        }
      })
   
export default Postagem;
