// imports para estilização
import styled from "styled-components/native"
import { globalStyles } from "../../global/global";

export const Cloud = styled.Image`
    width:102;
    height:70;
    margin-top:72;
    margin-left:155;
    display:flex;
    flex-direction:column;
    align-content:center;
`;
export const Perfil = styled.View`
    width:59;
    height:54;
    margin-top:15;
    margin-left:16;
    display:flex;
    flex-direction:column;
    align-content:center;
    border-radius: 100%;
    background: #FFBD00;
`;
export const FundoApp = styled.View`
    width:428;
    height:1435;
    display:flex;
    flex-direction:column;
    align-content:center;
    background: ${globalStyles.cores.jet};
`;
export const UpperBox = styled.View`
    width:428;
    height:389;
    flex-direction:column;
    align-content:center;
    margin-top: 34px;
    background: ${globalStyles.cores.default};
    border-radius:10;
`;
export const LowerBox = styled.View`
    width:428;
    height:636;
    margin-top: 31px;
    margin-bottom: 31px;
    flex-direction:column;
    align-content:center;
    background: ${globalStyles.cores.default};
    border-radius:10;

`;
export const ImageHolder = styled.View`
    width:410;
    height:240;
    flex-direction:column;
    align-content:center;
    margin-top: 106;
    left:10;
    background: #E6E6E6;
    position: absolute;
    border: 1px solid rgba(0, 0, 0, 0.29);
    border-radius: 5px;
`;
export const BtnWrapper = styled.View`
    align-self: center;
`;
export const TxtHeader = styled.Text`
    font-size:24;
    padding-left:20;
    padding-top:50;
    line-height: 18px;
    color: #FFFFFF;
    font-family:${globalStyles.fontes.mB700};
`;
export const TxtLabel = styled.Text`
    top: 269;
    font-size:14;
    padding-left:54;
    padding-top:12;
    line-height: 18px;
    color: rgba(0, 0, 0, 0.47);
    font-family:${globalStyles.fontes.mB700};
`;
export const Txt1 = styled.Text`
    height: 29;
    width: 205;
    font-size:15;
    padding-top:30;
    padding-left:20;
    line-height: 18px;
    color: #FFFFFF;
    font-family:${globalStyles.fontes.r400};
`;
export const Txt2 = styled.Text`
    left: 19;
    font-size:24;
    padding-top:35;
    padding-left:20;
    line-height: 18px;
    color: #FFFFFF;
    font-family:${globalStyles.fontes.mB700};
`;
export const Txt3 = styled.Text`
    left: 19;
    font-size:20;
    padding-top:20;
    padding-left:120;
    line-height: 20px;
    color: rgba(0, 0, 0, 0.58);
    font-family:${globalStyles.fontes.mB700};
`;
export const Txt4 = styled.Text`
    top: 269;
    font-size:14;
    padding-left:17;
    padding-top:35;
    line-height: 18px;
    color: #000000;
    font-family:${globalStyles.fontes.mB700};
`;
export const InfoWrapper = styled.View`
    flex-direction: row;
`;
export const Info = styled.View`
    flex-direction: column;
`;
