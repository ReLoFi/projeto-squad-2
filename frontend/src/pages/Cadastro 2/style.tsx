import styled from 'styled-components/native';

export const Background = styled.ImageBackground`
height:100%;
background-color:#39393A;
flex:1;
marginTop: StatusBar.currentHeight;
`
export const TextoGG = styled.Text`
font-size:36px;
font-weight: bold;
color:white;
`
export const TextSection = styled.View `
margin-bottom:55px;
align-items: center;
`
export const Button = styled.TouchableOpacity`
background-color: #FFB803;
margin-top: 49px;
width:317px;
height:56px;
align-items: center;
border-radius: 48px;
TouchableOpacity:30%

`
export const ButtonSection= styled.View`
align-items: center;
`
export const PassoSection=styled.View`
align-items: flex-end;
margin-right: 5%;
`

export const PassoText=styled.Text`
color: white;
font-size:16px
`
export const Texto = styled.Text`
font-size:16px;
font-weight:bold;
margin-top:14px;
`