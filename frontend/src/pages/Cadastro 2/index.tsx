import { useNavigation } from "@react-navigation/native";
import React from "react";
import { HeaderCadastro } from "../../components/headercadastro";
import {TextInput2} from "../../components/text input cadastro 2";
import { Background,TextoGG,TextSection,ButtonSection,Button, PassoSection,PassoText,Texto} from './style';


function Cadastro_2 () {
    const navigation = useNavigation();
    return(
      <Background source={require('../../../assets/Background Cadastro 1.png')}>
        <HeaderCadastro />
        <TextSection>
            <TextoGG>
            Endereço
        </TextoGG>
        </TextSection>
        <TextInput2/>
        <PassoSection>
          <PassoText>
            Passo 2/2
          </PassoText>
        </PassoSection>
        <ButtonSection>
            <Button onPress={()=>navigation.navigate('Login' as never)} >
                <Texto>Cadastrar</Texto>
            </Button>
            </ButtonSection>
      </Background>
      
      
    )
}

export default Cadastro_2;