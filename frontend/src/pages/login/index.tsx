import React from "react";
import {StyleSheet} from "react-native";
import {useNavigation} from '@react-navigation/native';
import {Background,LogoSection,Logo, Container,
ContaSection,Texto1,Texto2,Texto3, ButtonSection,Button,Texto4,SocialSection,IconSection} from './style'
import {TextInput3} from "../../components/text input login"
import Icon from 'react-native-vector-icons/AntDesign';

function Login () {
    const navigation = useNavigation();
    return (
        <Background source={require('../../../assets/Background violao Login.png')}>
            <Container>
                <LogoSection>
                <Logo source={require('../../../assets/EJCMusic.png')}/>
                </LogoSection>
                <TextInput3/>
                <ContaSection>
                    <Texto1>
                        Não possui conta?
                    </Texto1>
                    <Texto2 onPress={()=>navigation.navigate('Cadastro 1' as never)}>
                        Crie uma aqui.
                    </Texto2>
                </ContaSection>
                <ButtonSection>
                    <Button onPress={()=>navigation.navigate('Home' as never)}>
                        <Texto3>Entrar</Texto3>
                    </Button>
                </ButtonSection>
                <SocialSection>
                    <Texto4>
                        Ou entre usando:
                    </Texto4>
                    <IconSection>
                        <Icon style={style.google} name="google" size={60}> </Icon>
                        <Icon style={style.twitter} name="twitter" size={60}> </Icon>
                        <Icon style={style.facebook} name="facebook-square" size={60}> </Icon>
                    </IconSection>
                </SocialSection>
            </Container>
        </Background>
    )
}
const style= StyleSheet.create({
    google:{
        color:'#FFBD00'
    },
    twitter:{
        color:'#00ACEE'
    },
    facebook:{
        color:'#4267B2'
    },
})
export default Login;