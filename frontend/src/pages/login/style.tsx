import styled from 'styled-components/native';

export const Background = styled.ImageBackground `
flex:1;
background-color:#39393A;
width:100%;
height:100%;
`
export const Container = styled.View`
align-items:center;
`

export const LogoSection = styled.View`

margin-top:100px;
margin-bottom:79px;
align-items:center;
`
export const Logo = styled.Image`
height:71px;
width:387px;
// left:18px;
// right:16px;
`
export const ContaSection = styled.View`
flex-direction:row;
// padding-left:25%;
// align-items:center;
`

export const Texto1 = styled.Text`
color:white;
margin-right:5px;
`
export const Texto2 = styled.Text`
color:#FFBD00;
`
export const ButtonSection = styled.View`
align-items: center;
// padding-right:3%;
`

export const Button = styled.TouchableOpacity`
background-color: #FFB803;
margin-top: 43px;
width:299px;
height:56px;
align-items: center;
border-radius: 16px;
TouchableOpacity:30%
padding-top:15px;
`
export const Texto3 = styled.Text `
font-size:20px;
`

export const SocialSection = styled.View`
flex-direction:column;
align-items:center;
margin-top:50px;
`
export const Texto4 = styled.Text `
color:white;
`
export const IconSection = styled.View`
flex-direction:row;
margin-top:47px;

`