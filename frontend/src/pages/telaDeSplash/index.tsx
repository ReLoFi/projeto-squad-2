import * as React from 'react';
import { ImageBackground } from 'react-native';
import { Container, Logo } from './style';

export default function TelaDeSplash() { 
       
    return (
        <Container >
            <Logo source={require('../../../assets/EJCMusic.png')} />
        </Container>
    );
}
