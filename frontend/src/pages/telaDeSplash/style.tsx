import styled from 'styled-components/native';
import { globalStyles } from '../../global/global';

export const Container = styled.View`
    background: ${globalStyles.cores.jet};
    flex: 1;
    width: 100%;
    justify-content: center;
`;


export const Logo = styled.ImageBackground`
    width: 90.5%;
    margin-bottom: 13.17%;
    aspect-ratio: 5.6;
    margin-left: 9%;
    margin-bottom: 61.5%;
`;

