import * as React from 'react';
import { AiOutlineArrowLeft, AiOutlineUser } from 'react-icons/ai';
import {BackgroundApp, TxtHeader, InfoWrapper, Info, PerfilIcon, TxtSize36,TxtAdjustedPadding, HeaderView, HistoryView, ProductCard, ProductImg, TxtSize12, TxtSize20, BtnWrapper, Botoes} from '../Perfil/style';
import Botao from '../../components/Botao';
import InfoLabel from '../../components/InputBlocked';
import { useNavigation } from '@react-navigation/native';
import { TouchableOpacity } from 'react-native';

function Perfil() {
    const navigation = useNavigation();  

    return (
        <BackgroundApp>

            {/* Cabeçalho da página */}
            <HeaderView source={require('../../../assets/background.svg')}/>
            <TouchableOpacity  onPress={() => navigation.goBack()}>
                <InfoWrapper>
                        <AiOutlineArrowLeft  style={styles.arrow}/>
                        <TxtHeader> Meu Perfil </TxtHeader>
                </InfoWrapper>
            </TouchableOpacity>
            
            {/* Informações do conteúdo da página */}
            <InfoWrapper>
                {/* Icone do usuário */}
                <Info>
                    <PerfilIcon>
                        <AiOutlineUser style={styles.userIcon}/>
                    </PerfilIcon>
                </Info>

                <Botoes>
                    <BtnWrapper onPress={() => navigation.navigate('Postagem' as never)}>
                        <Botao value='Postar Produto'/>
                    </BtnWrapper>

                    {/* Editar perfil */}
                    <BtnWrapper>
                        <Botao value='Editar Perfil'/>
                    </BtnWrapper>
                </Botoes>
                
            </InfoWrapper>
 
            {/* Título */}
            <InfoWrapper>
                    <TxtSize36> Dados Pessoais</TxtSize36>
            </InfoWrapper>

            {/* Informações do usuário */}
            <InfoLabel param={"Ciclano da Silva"}/>
            <InfoLabel param={"ciclanodasilva01@gmail.com"}/>
            <InfoLabel param={"01/02/2000"}/>

            {/* Área de histórico de busca e favoritos */}
            <HistoryView>

                {/* Categorias visitadas e último produto */}
                {/* Histórico favoritos */}
                <Info>

                    <TxtAdjustedPadding>Favoritos</TxtAdjustedPadding>
                    {/* Card do produto */}
                    <ProductCard>
                        <ProductImg source={require('../../../assets/Violão.png')}/>
                        <TxtSize12>
                        Violão Elétrico Aço Roadster Parlor TWR2 P E Natural Tanglewood
                        </TxtSize12>
                        <TxtSize12>
                        R$ 2.479,00
                        </TxtSize12>
                    </ProductCard>

                </Info>

                {/* Histórico últimos produtos visitados */}
                <Info>
                    <TxtSize20>Vistos Recentemente</TxtSize20>
                    <ProductCard>
                        <ProductImg source={require('../../../assets/Violão.png')}/>
                        <TxtSize12>
                        Violão Elétrico Aço Roadster Parlor TWR2 P E Natural Tanglewood
                        </TxtSize12>
                        <TxtSize12>
                        R$ 2.479,00
                        </TxtSize12>
                    </ProductCard>

                </Info>
                
            </HistoryView>

        </BackgroundApp>
    )}

    // Estilização dos icones
    const styles = ({
        userIcon: {
            height: 72,
            width: 73,
            marginLeft:25,
            marginTop: 24,
            color:'black',
        },
        arrow:{
            height: 40,
            width: 40,
            marginLeft:29,
            paddingTop:40,
            color:'#FFBD00',
        }
      })
   
export default Perfil;