// imports para estilização
import styled from "styled-components/native"
import { globalStyles } from "../../global/global";

export const HeaderView = styled.ImageBackground`
    position: absolute;
    width: 100%;
    height: 591;
    left: -10px;
    top: -411px;
`;
export const BtnWrapper = styled.TouchableOpacity`
    left: 18.69158%;
    bottom:7.47663%;
`;

export const Botoes = styled.View`
    flex-direction: column;
    align-items: center;
    align-self: flex-end;
    gap: 11px;
    margin-right: 40px;
    
`;

export const PerfilIcon = styled.View`
    width:123;
    height:120;
    margin-top:15;
    margin-left:16;
    display:flex;
    flex-direction:column;
    align-content:center;
    border-radius: 100%;
    background: #FFBD00;
    display:flex;
`;

export const BackgroundApp = styled.View`
    width:100%;
    height:926;
    display:flex;
    flex-direction:column;
    align-content:center;
    background: ${globalStyles.cores.jet};
`;
export const TxtHeader = styled.Text`
    font-size:24;
    padding-left:20;
    padding-top:50;
    line-height: 18px;
    color: #000000;
    font-family:${globalStyles.fontes.m600};
`;
export const TxtSize12 = styled.Text`
    height: 40;
    width: 180;
    font-size:12;
    line-height: 18px;
    color: '#0000000';
    margin-top: 13;
    padding-left: 10;
    font-family:${globalStyles.fontes.r400};
`;
export const TxtAdjustedPadding = styled.Text`
    left: 19;
    font-size:20;
    padding-top: 25%;
    padding-bottom:14;
    padding-left:20;
    line-height: 18px;
    width:170;
    text-align: center;
    color: #000000;
    font-family:${globalStyles.fontes.r400};
`;
export const TxtSize20 = styled.Text`
    left: 19;
    font-size:20;
    padding-top:16.2%;
    padding-bottom:14;
    padding-left:20;
    line-height: 18px;
    width:170;
    text-align: center;
    color: #000000;
    font-family:${globalStyles.fontes.r400};
`;
export const TxtSize36 = styled.Text`
    top: 269;
    font-size:36;
    padding-left:70;
    padding-top:35;
    padding-bottom:45;
    line-height: 18px;
    color: #FFFFFF;
    font-family:${globalStyles.fontes.m500};
`;
export const InfoWrapper = styled.View`
    flex-direction: row;
`;
export const Info = styled.View`
    flex-direction: column;
`;
export const HistoryView = styled.View`
    background: ${globalStyles.cores.amber};
    flex-direction: row;
    padding-left:17;
    position: absolute;
    width: 100%;
    height: 361px;
    top: 630px;
    border-top-left-radius: 25px;
    border-top-right-radius: 25px;
`;
export const ProductCard = styled.View`
    background: ${globalStyles.cores.default};
    flex-direction: column;
    margin-bottom:17.1745%;
    margin-right:20;
    height: 210px;
    width: 186;
    border-radius: 25px;
`;
export const ProductImg = styled.Image`
    height: 118px;
    width: 186px;
`;