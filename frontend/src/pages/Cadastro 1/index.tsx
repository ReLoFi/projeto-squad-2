import React from "react";
import { HeaderCadastro } from "../../components/headercadastro";
import { TextInput } from "../../components/text input cadastro 1"
import { useNavigation } from '@react-navigation/native';
import {Background, TextoGG,TextSection,ButtonSection,Button, PassoSection,PassoText,Texto} from './style';

function Cadastro_1 () {
    const navigation = useNavigation();
    return(
      
        <Background source= {require('../../../assets/Background Cadastro 1.png')}>
            <HeaderCadastro/>
            <TextSection>
                <TextoGG>
                Dados Pessoais
            </TextoGG>
            </TextSection>
            <TextInput/>
            <PassoSection>
                <PassoText>
                    Passo 1/2
                </PassoText>
            </PassoSection>
            <ButtonSection>
                <Button onPress={()=>navigation.navigate('Cadastro 2' as never)}>
                    <Texto>Avançar</Texto>
                </Button>
            </ButtonSection>
            
        </Background>
        
      
    )
}

export default Cadastro_1;