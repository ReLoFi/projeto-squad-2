import * as React from 'react';
import { BotaoPostarComentario, FundoAvaliacao, ListaComentarios, TextoBotao, Titulo, FundoImagem, ImagemProduto, BotaoVoltar, Fundo } from './style';
import { AiOutlineArrowLeft } from 'react-icons/ai';
import { globalStyles } from '../../global/global';
import Comentario from '../../components/Comentario';
import Estrelas from '../../components/Estrelas';
import { useNavigation } from '@react-navigation/native';
import InfoProduto from '../../components/InfoProduto';

export default function TelaDeProduto() { 
    const navigation = useNavigation();
    
    return (
        <Fundo>
            {/* Seta de voltar */}
            <BotaoVoltar onPress={() => navigation.goBack() }>
                <AiOutlineArrowLeft style={{ fontSize: '40px' ,color: `${globalStyles.cores.amber}` }}/>
            </BotaoVoltar> 


            {/* Imagem do produto */}
            <FundoImagem> 
                <ImagemProduto source={require(`../../../assets/Violao.jpg`)}/>
            </FundoImagem>

            {/* Iformação do produto */}
            <InfoProduto nome='Violão Elétrico Aço Roadster Parlor TWR2 P E Natural Tanglewood' preco={2479.00} descricao='QUE É UM VIOLÃO PARLOR?
        Violões Parlor possuem dimensões físicas menores se comparados aos violões mais populares e se destacam bastante nos palcos e na parede de nossa loja diantes dos instrumentos mais convencionais.'/>

             {/* Avaliação do produto */}
            <FundoAvaliacao>
                <Titulo>Avaliações de clientes</Titulo>
                <Estrelas QuantideCheias = {4} QuantideVazias = {1} Media={4} />
                <ListaComentarios>
                    <Comentario Nome='Ciclano Da Silva' FotoUrl='ProfilePicture.png' QuantideCheias = {4} QuantideVazias = {1} Comentario='Violão muito bom.Mais barato que as lojas fisicas Violão muito bom.Mais barato que as lojas fisicas'></Comentario>
                    <Comentario Nome='Ciclano Da Silva' FotoUrl='ProfilePicture.png' QuantideCheias = {4} QuantideVazias = {1} Comentario='Violão muito bom.Mais barato que as lojas fisicas Violão muito bom.Mais barato que as lojas fisicas'></Comentario>
                    <Comentario Nome='Ciclano Da Silva' FotoUrl='ProfilePicture.png' QuantideCheias = {4} QuantideVazias = {1} Comentario='Violão muito bom.Mais barato que as lojas fisicas Violão muito bom.Mais barato que as lojas fisicas'></Comentario>
                </ListaComentarios>
                <BotaoPostarComentario>
                <TextoBotao>
                    Escrever uma avaliação 
                </TextoBotao>
                </BotaoPostarComentario>
            </FundoAvaliacao>
        </Fundo>
    );
}