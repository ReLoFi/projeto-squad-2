import styled from 'styled-components/native';
import { globalStyles } from '../../global/global';

export const Fundo = styled.ScrollView`
    background: ${globalStyles.cores.jet};
    flex: 1;
`;

export const BotaoVoltar = styled.TouchableOpacity`
    margin-top: 40px;
    margin-left: 26px;
`;

export const FundoImagem = styled.View`
    width: 100%;
    height: 25%;
    background-color: ${globalStyles.cores.gray};
    margin-top: 31px;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    border-radius: 5px;
`;

export const ImagemProduto = styled.Image`
    height: 100%;
    width: auto;
    aspect-ratio: 1;
    align-self: center;
`;

export const FundoAvaliacao = styled.View`
    width: 100%;
    background-color: ${globalStyles.cores.gray};
    margin-top: 39px;
    border-radius: 10px;
    padding-bottom: 124px;
    margin-bottom: 101px;
`;
  
export const Titulo = styled.Text`
    font-size: 24px;
    color: black;
    font-family: ${globalStyles.fontes.r400};
    margin-top: 25px;
    margin-left: 13px;
`;
  
export const BotaoPostarComentario = styled.TouchableOpacity`
    background-color: ${globalStyles.cores.amber};
    align-items: center;
    justify-content: center;
    align-self: center;
    width: 329px;
    height: 48px;
    border-radius: 16px;
    margin-top: 7px;
`;

export const TextoBotao = styled.Text`
    font-size: 24px;
    color: black;
`;


export const ListaComentarios = styled.View`
    flex-direction: column;
    align-items: center;
    gap: 10px;
`;