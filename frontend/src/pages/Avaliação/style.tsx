// imports para estilização
import styled from "styled-components/native"
import { globalStyles } from "../../../src/global/global";


export const Perfil = styled.View`
    width:59;
    height:54;
    margin-top:15;
    margin-left:16;
    display:flex;
    flex-direction:column;
    align-content:center;
    border-radius: 100%;
    background: #FFBD00;
`;
export const FundoApp = styled.View`
    width:428;
    height:926;
    display:flex;
    flex-direction:column;
    align-content:center;
    background: ${globalStyles.cores.jet};
`;
export const Box = styled.View`
    width:428;
    height:493;
    flex-direction:column;
    align-content:center;
    margin-top: 216;
    background: ${globalStyles.cores.default};
    position: absolute;
    border-radius:10;
`;
export const BtnWrapper = styled.View`
    height: 52;
    width: 260;
    left: 14;
    top: 716;
    border-radius: 0;
    flex-direction:column;
    align-content:center;
    position: absolute;
    bottom:0;
`;
export const TxtHeader = styled.Text`
    font-size:24;
    padding-left:20;
    padding-top:50;
    line-height: 18px;
    color: #FFFFFF;
    font-family:${globalStyles.fontes.mB700};
`;
export const Txt1 = styled.Text`
    height: 29;
    width: 205;
    font-size:15;
    padding-top:30;
    padding-left:20;
    line-height: 18px;
    color: #FFFFFF;
    font-family:${globalStyles.fontes.r400};
`;
export const Txt2 = styled.Text`
    left: 19;
    font-size:20;
    padding-top:22;
    padding-bottom:14;
    padding-left:20;
    line-height: 18px;
    color: #000000;
    font-family:${globalStyles.fontes.r400};
`;
export const Txt4 = styled.Text`
    top: 269;
    font-size:14;
    padding-left:17;
    padding-top:35;
    line-height: 18px;
    color: #000000;
    font-family:${globalStyles.fontes.mB700};
`;
export const InfoWrapper = styled.View`
    flex-direction: row;
`;
export const Info = styled.View`
    flex-direction: column;
`;
export const Stars = styled.View`
    flex-direction: row;
    padding-left:17;
`;