import * as React from 'react';
import { AiOutlineArrowLeft, AiOutlineStar, AiOutlineUser } from 'react-icons/ai';
import {FundoApp, Box, TxtHeader, BtnWrapper, Txt1, Txt2, InfoWrapper, Info, Perfil, Txt4, Stars} from './style';
import Botao from '../../components/Botao';
import InputTexto from '../../components/InputTexto';

function Avaliação() {

    return (
        <FundoApp>
            {/* Cabeçalho da página */}
            <InfoWrapper>
                <AiOutlineArrowLeft style={styles.arrow}/>
                <TxtHeader> Avaliar </TxtHeader>
            </InfoWrapper>

            {/* Informações do conteúdo da página */}
            <InfoWrapper>
                <Info>
                    <Txt1> Marketplace</Txt1>
                </Info>
                <Botao value='Salvar rascunho'/>
                
            </InfoWrapper>
            
            {/* Área de inserção da avaliação */}
            <Box>
                {/* Perfil que está fazendo a avaliação */}
                <InfoWrapper>
                    <Perfil>
                        <AiOutlineUser style={styles.userIcon}/>
                    </Perfil>
                    <Txt4> Ciclano da Silva</Txt4>
                </InfoWrapper>

                <Txt2>Classificação</Txt2>

                <Stars>
                    <AiOutlineStar style={styles.star}/>
                    <AiOutlineStar style={styles.star}/>
                    <AiOutlineStar style={styles.star}/>
                    <AiOutlineStar style={styles.star}/>
                    <AiOutlineStar style={styles.star}/>
                </Stars>

                <InputTexto param={"Título"}/>
                <InputTexto param={"Descrição"}/>

            </Box>

            {/* Botão de confirmação */}
            <BtnWrapper>
            <Botao value='Confirmar'/>
            </BtnWrapper>

        </FundoApp>
    )}

    // Estilização dos icones
    const styles = ({
        userIcon: {
            height: 35,
            width: 34,
            marginLeft:13,
            marginTop: 10,
            color:'black',
        },
        arrow:{
            height: 40,
            width: 40,
            marginLeft:29,
            paddingTop:40,
            color:'#FFBD00',
        },
        star:{
            height: 24,
            width: 24,
        }
      })
   
export default Avaliação;
