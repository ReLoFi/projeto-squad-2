import styled from 'styled-components/native';
import { globalStyles } from '../../global/global';

export const Fundo1 = styled.View`
    background: ${globalStyles.cores.jet};
    flex: 1;
    height: 100%;
    width: 100%;
`;


export const Cabeca = styled.View`
    flex-direction: row;
    align-items: center;
    flex-wrap: wrap;
    margin-left: 14px;
    margin-top: 19px;
    gap: 14px;
`;

export const BotaoVoltar = styled.TouchableOpacity`
    
`;

export const Titulo = styled.Text`
    font-family: ${globalStyles.fontes.r400};
    color: white;
    font-size: 24px;
`;

export const Logo = styled.Image`
    width: 290px;
    height: 56px;
    align-self: center;
    margin-top: 4px;
    margin-bottom: 11px;
`;


export const Fundo2 = styled.View`
    flex: 1;
    background: ${globalStyles.cores.gray};
    width: 100%;
    height: 77%;
    margin-top: 23px;
`;


export const ImagemFundo = styled.ImageBackground`
    width: 100%;
    height: 100%;
`;


export const Busca = styled.View`
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
`;

export const Ordenacao = styled.View`
    flex-direction: row;
    align-items: center;
    margin-left: 34px;
    margin-top: 24px;

`;

export const Nome = styled.Text`
    font-family: ${globalStyles.fontes.m500};
    font-size: 14px;
    color: black;
    margin-left: 2px;
    margin-right: 8px;
`;

export const NomeFiltro = styled.Text`
    font-family: ${globalStyles.fontes.m500};
    font-size: 14px;
    color: rgba(0, 0, 0, 0.58);
`;

export const Filtro = styled.View`
    flex-direction: row;
    align-items: center;
    margin-right: 27px;
    margin-top: 21px;
    gap: 5px;
`;

export const ListaProdutos = styled.View`
    padding-top: 25px;
    padding-bottom: 85px;
    gap: 20px;
    flex-direction: collum;
    justify-content: flex-start;
    align-items: center;
`;

