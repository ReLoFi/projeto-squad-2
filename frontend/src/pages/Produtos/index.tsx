import { useNavigation } from '@react-navigation/native';
import * as React from 'react';
import { AiOutlineArrowLeft } from 'react-icons/ai';
import {Image, ScrollView } from 'react-native';
import { BuscaBarra } from '../../components/BuscaBarra';
import { CaixaProduto } from '../../components/CaixaProduto';
import { produtos } from '../../constants/produtos';
import { useSearchValue } from '../../contexts/search';
import { globalStyles } from '../../global/global';
import { BotaoVoltar, Busca, Cabeca, Filtro, Fundo1, Fundo2, ImagemFundo, ListaProdutos, Logo, Nome, NomeFiltro, Ordenacao, Titulo } from './style';


export default function Produtos() {
  const navigation = useNavigation();
  const searchValue = useSearchValue();
   
  return (
    <Fundo1>
      {/*Cabeçalho */}
      <Cabeca>
        <BotaoVoltar onPress={() => navigation.goBack() }>
          <AiOutlineArrowLeft style={{ fontSize: '40px' ,color: `${globalStyles.cores.amber}` }}/>
        </BotaoVoltar> 
        <Titulo>Produtos</Titulo>
      </Cabeca>

      <Logo source={require('../../../assets/EJCMusic.png')} />

      <BuscaBarra/>

      {/* fundo dos produtos */}
      <Fundo2> 
        <ImagemFundo source={require('../../../assets/ImagemFundo.svg')}>


          {/* Filtros de busca */}
          <Busca>

            <Ordenacao> 
              <Image source={require('../../../assets/sort.png')} 
                style={{width: '22px', height: '18px'}}
              />
              <Nome>Ordenar</Nome>
              <NomeFiltro>em promoção</NomeFiltro>

            </Ordenacao>

            <Filtro> 
              <Image source={require('../../../assets/Filter.png')} 
                  style={{width: '22px', height: '18px'}}
                />
                <Nome>Filtrar</Nome>
            </Filtro>

          </Busca>

          {/* Lista de produtos */}
          <ScrollView> 
            <ListaProdutos> 
            {
              produtos.map((produto) => {
                if(produto.nome.toLowerCase().includes(searchValue.toLowerCase())){
                  return (
                    <CaixaProduto 
                      nome = {produto.nome}
                      preco = {produto.preco}
                      nomeImagem = {produto.nomeImagem}
                    />
                  )
                } 
              }
            )}              
            </ListaProdutos>
          </ScrollView>
        </ImagemFundo>
      </Fundo2>

    </Fundo1>
    );
  }
 