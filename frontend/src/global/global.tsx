export const globalStyles = {
    cores: {
        amber: "#FFBD00",
        jet : "#39393A",
        gainsboro : '#DAE3E5',
        gray: '#d9d9d9',
        Buckthorn:'#FCA92C',  
        default: "#D9D9D9",
    },

    fontes: {
       m400: "Montserrat_400Regular", 
       m500: "Montserrat_500Medium",
       m600: "Montserrat_600SemiBold",
       r400: "Roboto_400Regular",
       mB700: "Montserrat_700Bold",
       e700: "Epilogue_700Bold",
    }
    
}
