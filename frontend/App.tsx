import * as React from 'react';
import { useFonts,Montserrat_500Medium, Montserrat_600SemiBold,Montserrat_700Bold, Montserrat_400Regular} from '@expo-google-fonts/montserrat';
import { Roboto_400Regular, } from '@expo-google-fonts/roboto';
import { Epilogue_700Bold } from '@expo-google-fonts/epilogue';
import Router from './src/Router/router';
import { SearchProvider } from './src/contexts/search';



export default function App() {

  let [fontsLoaded] = useFonts({
    Montserrat_400Regular,
    Montserrat_500Medium,
    Montserrat_600SemiBold ,
    Montserrat_700Bold,
    Roboto_400Regular,
    Epilogue_700Bold,
  });

  return (
    <>
      <SearchProvider>
        <Router/>
      </SearchProvider>
    </>

  );
}
