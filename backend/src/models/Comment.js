const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const Comment = sequelize.define('Comment', {

    content:{
        type: DataTypes.STRING, 
        allowNull: false
    },

    evaluation:{
        type: DataTypes.DOUBLE,
        allowNull: false
    }

});

Comment.associate = function(models) {
    Comment.belongsTo(models.Product)
    Comment.belongsTo(models.User);
};

module.exports = Comment;