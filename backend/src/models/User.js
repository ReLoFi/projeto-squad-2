const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const User = sequelize.define('User',{
    name:{
        type: DataTypes.STRING,
        allowNull: false
    },
    cpf:{
        type: DataTypes.STRING,
        allowNull: false
    },
    birthday:{
        type: DataTypes.DATEONLY,
        allowNull: false
    },
    email:{
        type: DataTypes.STRING,
        allowNull: false
    },
    isModerator:{
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    hash: {
        type: DataTypes.STRING,
    },
    salt: {
        type: DataTypes.STRING,
    }


},{
    timestamps: false
});

User.associate = function(models){
    User.hasOne(models.Adress);
    User.hasMany(models.Comment);
    User.hasMany(models.Product);
    User.belongsToMany(models.Product, {
        through: 'Cart',
        as: 'Buyer',
        foreignKey: 'buyerId'
    });
    User.belongsToMany(models.Product, {
        through: 'Favorites',
        as: 'Favoriting'
    })

}

module.exports = User;
