const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const Adress = sequelize.define('Adress',{
    cep:{
        type: DataTypes.STRING,
        allowNull: false
    },
    street:{
        type: DataTypes.STRING,
        allowNull: false
    },
    streetNumber:{
        type: DataTypes.INTEGER,
        allowNull: false
    },
    city:{
        type: DataTypes.STRING,
        allowNull: false
    },
    state:{
        type: DataTypes.STRING,
        allowNull: false
    }
},{
    timestamps: false
});

Adress.associate = function(models){
    Adress.belongsTo(models.User);
}

module.exports = Adress;
