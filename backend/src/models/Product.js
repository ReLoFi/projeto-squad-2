const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const Product = sequelize.define('Product', {

    name:{
        type: DataTypes.STRING, 
        allowNull: false
    },

    price:{
        type: DataTypes.DOUBLE,
        allowNull: false

    },

    description:{
        type: DataTypes.STRING,
        allowNull: false
    },

    category:{
        type: DataTypes.STRING,
        alllowNull: false

    },
});

Product.associate = function(models) {
    Product.hasMany(models.Comment);
    Product.hasMany(models.Photo);
    Product.belongsTo(models.User);
    Product.belongsToMany(models.User, {
        through: 'Cart',
        as: 'Buyer',
        foreignKey: 'boughtId'
    });
    Product.belongsToMany(models.User, {
        through: 'Favorites',
        as: 'Favoriting',
    });
};

module.exports = Product;