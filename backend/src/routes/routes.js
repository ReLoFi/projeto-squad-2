const { Router } = require('express');
const ProductController = require('../controllers/ProductController');
const CommentController = require('../controllers/CommentController');
const UserController = require('../controllers/UserController');
const AdressController = require('../controllers/AdressController');
const CartController = require('../controllers/CartController');
const FavoritesController = require('../controllers/FavoritesController');
const router = Router();
const path = require('path');
const multer = require('multer');
const storage = require("../config/files");
const validator = require("../config/Validator");
const ownerMiddleware = require("../middlewares/owner");
const moderatorMiddleware = require("../middlewares/moderator"); 
const setAuthorizationHeader = require('../middlewares/token');
const AuthController = require("../controllers/AuthController");
const passport = require("passport");
router.use("/private", passport.authenticate('jwt', {session: false}));

const upload = multer({ storage: storage,
	fileFilter: function (req, file, cb) {
	        const ext = path.extname(file.originalname);
	        if(ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
	            return cb(new Error('Erro extensão não suportada!'), false);
	        }
	        cb(null, true);
	    },
	    limits:{
	        fileSize: 2048 * 2048
	    }

 });

//rotas de autentiação

 router.post("/login", AuthController.login);
 router.get("/private/getDetails", AuthController.getDetails);

//rotas de foto

router.post('/products/photo/:id', upload.single('photo'), ProductController.addPhoto);
router.delete('/photo/:id', ProductController.removePhoto);

//rotas de produto

router.get('/products', ProductController.index);
router.get('/products/:id',  ProductController.show);
router.post('/products', validator.validationProduct('create'), ProductController.create);
router.put('/products/:id', validator.validationProduct('update'), ProductController.update);
router.delete('/products/:id', ProductController.destroy);
router.put('/products/adduser/:id', ProductController.addUser);
router.delete('/products/removeuser/:id', ProductController.removeUser);

//rotas de comentário

router.get('/comments', CommentController.index);
router.get('/comments/:id', CommentController.show);
router.post('/comments', validator.validationComment('create'), CommentController.create);
router.put('/comments/:id', validator.validationComment('update'), CommentController.update);
router.delete('/comments/:id', CommentController.destroy);
router.put('/comments/addproduct/:id', CommentController.addProduct);
router.delete('/comments/removeproduct/:id', CommentController.removeProduct);
router.put('/comments/adduser/:id', CommentController.addUser);
router.delete('/comments/removeuser/:id', CommentController.removeUser);

//rotas de usuário/moderador

router.get('/users',UserController.index);
router.get('/users/:id',UserController.show);
router.post('/users', validator.validationUser('create'), UserController.create);
router.put('/users/:id', validator.validationUser('update'), UserController.update);
router.delete('/users/:id', UserController.destroy);
router.put('/users/useraddadress/:id', UserController.addRelationAdress);
router.put('/users/userremoveadress/:id', UserController.removeRelationAdress);
router.put('/users/addincart/:id', CartController.addProductInCart);
router.put('/users/removefromcart/:id', CartController.removeProductFromCart);
router.get('/users/listcart/:id', CartController.listProductsInCart);
router.put('/users/addinfavorites/:id', FavoritesController.addProductsInFavorites);
router.put('/users/removefromfavorites/:id', FavoritesController.removeProductfromFavorites);
router.get('/users/listinfavorites/:id', FavoritesController.listFavorites);
router.delete('/users/deletecomment/:id', ownerMiddleware.commentOwner, UserController.userDeleteComment);
router.delete('/users/deleteproduct/:id', ownerMiddleware.productOwner, UserController.userDeleteProduct);
router.delete('/moderator/deletecomment/:id', moderatorMiddleware.moderatorValidation, UserController.userDeleteComment);
router.delete('/moderator/deleteproduct/:id', moderatorMiddleware.moderatorValidation, UserController.userDeleteProduct);

//rotas de endereço

router.get('/adresses',AdressController.index);
router.get('/adresses/:id',AdressController.show);
router.post('/adresses', validator.validationAdress('create'), AdressController.create);
router.put('/adresses/:id', validator.validationAdress('update'), AdressController.update);
router.delete('/adresses/:id', AdressController.destroy);

module.exports = router;