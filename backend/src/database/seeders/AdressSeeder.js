const Adress = require("../../models/Adress");
const faker = require('faker-br');

const seedAdress = async function () {
  try {
    await Adress.sync({ force: true });

    for (let i = 0; i < 10; i++) {
      await Adress.create({
        cep: faker.random.number(),
        street: faker.lorem.word(),
        streetNumber: faker.random.number(),
        city: faker.lorem.word(),
        state: faker.lorem.word()

      });
    }
  } catch (err) {
    console.log(err);
  }
};

module.exports = seedAdress;