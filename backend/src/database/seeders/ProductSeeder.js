const Product = require("../../models/Product");
const faker = require('faker-br');

const seedProduct = async function () {
  try {
    await Product.sync({ force: true });

    for (let i = 0; i < 10; i++) {
      await Product.create({
        name: faker.commerce.productName(),
        description: faker.lorem.text(),
        price: faker.commerce.price(30, 800, 2),
        category: faker.commerce.productMaterial()
      });
    }
  } catch (err) {
    console.log(err);
  }
};

module.exports = seedProduct;