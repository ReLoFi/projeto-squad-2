require('../../config/dotenv')();
require('../../config/sequelize');



const seedUser = require('./UserSeeder');
const seedProduct = require('./ProductSeeder');
const seedAdress = require('./AdressSeeder');
const seedComment= require('./CommentSeeder');

(async () => {
  try {

    await seedUser();
    await seedProduct();
    await seedAdress();
    await seedComment();

  } catch(err) { console.log(err) }
})();
