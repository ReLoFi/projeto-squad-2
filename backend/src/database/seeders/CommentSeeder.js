const Comment = require("../../models/Comment");
const faker = require('faker-br');

const seedComment = async function () {
  try {
    await Comment.sync({ force: true });

    for (let i = 0; i < 10; i++) {
      await Comment.create({
        content: faker.lorem.text(),
        evaluation: faker.random.number()
      });
    }
  } catch (err) {
    console.log(err);
  }
};

module.exports = seedComment;