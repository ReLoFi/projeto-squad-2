const Product = require('../models/Product');
const User = require('../models/User');
const Comment = require('../models/Comment');

//valida se o usuário é dono do comentário 

const commentOwner = async(req, res, next) =>{
    const {id} = req.params
    try{
        const user = await User.findByPk(id);
        const comment = await Comment.findByPk(req.body.CommentId)
        if (user.id == comment.UserId)
        return next();
        else return res.status(401).json({'error': 'Sem autorização'});
    } catch(err) {
        return res.status(500).json({'error': 'Internal Server Error'});
    };
}

//valida se o usuário é dono do produto

const productOwner = async(req, res, next) =>{
    const {id} = req.params
    try{
        const user = await User.findByPk(id);
        const product = await Product.findByPk(req.body.ProductId)
        if (user.id == product.UserId)
        return next();
        else return res.status(401).json({'error': 'Sem autorização'});
    } catch(err) {
        return res.status(500).json({'error': 'Internal Server Error'});
    };
}

module.exports = {
    commentOwner,
    productOwner
}
