const { validationResult } = require('express-validator');
const { response } = require('express');
const Comment = require('../models/Comment');
const Product = require('../models/Product');
const User = require('../models/User');

const create = async(req, res) => {
    
    try{
        const comment = await Comment.create(req.body);
        return res.status(201).json({message: "Comentário feito com sucesso!", comment});
    }catch(err){
        res.status(500).json({error: err});
    }
};

const index = async(req, res) => {
    try{
        const comments = await Comment.findAll();
        return res.status(200).json({comments});
    }catch(err) {
        return res.status(500).json({err});
    }
    };

const show = async(req, res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        return res.status(200).json({comment});
    }catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req, res) => {
    const {id} = req. params;
    try {
        const [updated] = await Comment.update(req.body, {where: {id: id}}); 
        if(updated) {
            const comment = await Comment.findByPk(id);
            return res.status(200).send(comment);
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Comentário não encntrado");
    }
};

const destroy = async(req, res) => {
    const {id} = req.params;
    try{
        const deleted = await Comment.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Comentário deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Comentário não encontrado.");
    }
};

//adiciona produto

const addProduct = async(req, res) => {
    const {id} = req.params;
    try{
        const comment = await Comment.findByPk(id);
        const product = await Product.findByPk(req.body.ProductId);
        await comment.setProduct(product);
        return res.status(200).json(product);
    }catch(err){
        return res.status(500).json({err});
    }
};

//remove produto

const removeProduct = async(req, res) => {
    const {id} = req.params;
    try{
        const comment = await Comment.findByPk(id);
        await comment.setProduct(null);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
};

//adiciona usuário

const addUser = async(req, res) => {
    const {id} = req.params;
    try{
        const comment = await Comment.findByPk(id);
        const user = await User.findByPk(req.body.UserId);
        await comment.setUser(user); 
        return res.status(200).json(user);
    }catch(err){
        return res.status(500).json({err});
    }
};

//remove usuário

const removeUser = async(req, res) => {
    const {id} = req.params;
    try{
        const comment = await Comment.findByPk(id);
        await comment.setUser(null);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
};

module.exports = {
    create,
    index,
    show,
    update,
    destroy,
    addProduct,
    removeProduct, 
    addUser,
    removeUser,
};