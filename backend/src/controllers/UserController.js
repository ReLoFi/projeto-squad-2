const { validationResult } = require('express-validator');
const User = require('../models/User');
const Adress = require('../models/Adress');
const Product = require('../models/Product');
const Comment = require('../models/Comment');
const mailer = require('../config/mail').mailer;
const readHtml = require("../config/mail").readHTMLFile;
const path = require('path');
const hbs = require("handlebars");
const Auth = require("../config/Auth");


//cria um usuário
const create = async(req,res)=>{
    try{

        validationResult(req).throw();
        const { password } = req.body;
        const HashSalt = Auth.generatePassword(password);
        const salt = HashSalt.salt;
        const hash = HashSalt.hash
        
        const pathTemplate = path.resolve(__dirname, '..', '..', 'templates');
		console.log(pathTemplate);

        const newUser = {
            name: req.body.name,
            email: req.body.email,
            birthday: req.body.birthday,
            cpf: req.body.cpf,
            isModerator: req.body.isModerator,
            hash: hash,
            salt: salt
        };       
        const user = await User.create(newUser);
        
        readHtml(path.join(pathTemplate, "email.html"), (err,html)=>{
			const template = hbs.compile(html);
			const replacements = {
				name: user.name
			};
			const htmlToSend = template(replacements);
        const message = {
            to: user.email,
            subject: 'Cadastro no EJCMusic',
            html: htmlToSend
        }
        mailer.sendMail(message, (err) => {
            console.log(err + "!");
        })
    });
        return res.status(201).json({message: "Usuário cadastrado com sucesso!", user: user});
    }catch(err){
        res.status(500).json(err+'');
    }
};

//mostra os usuários
const index = async(req,res)=>{
    try{
        const users = await User.findAll();
        return res.status(200).json({users});
    }catch(err){
        res.status(500).json({err});
    }
};

//mostra um usuário
const show = async(req,res)=>{
    const{id} = req.params;
    try{
        const user = await User.findByPk(id);
        return res.status(200).json({user});
    }catch(err){
        res.status(500).json({err});
    }
};

//edita um usuário
const update = async(req,res)=>{
    const{id} = req.params;
    try{
        const [updated] = await User.update (req.body, {where: {id: id}});
        if(updated){
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não encontrado");
    }
};

//deleta um usuário
const destroy = async(req,res)=>{
    const {id} = req.params;
    try{
        const deleted = await User.destroy({where: {id: id}});
        if(deleted){
            return res.status(200).json("Usuário deletado com sucesso.");
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não encontrado.");
    }
};

//adciona o id do endereço ao usuário
const addRelationAdress = async(req,res) => {
    const {id} = req.params;
    try {
        console.log(id);
        const user = await User.findByPk(id);
        const adress = await Adress.findByPk(req.body.AdressId);
        await user.setAdress(adress);
        return res.status(200).json(adress);
    }catch(err){
        return res.status(500).json({err});
    }
};

//remove o id do cliente do produto

const removeRelationAdress = async(req,res) => {
    const {id} = req.params;
    console.log(id);
    try {
        const user = await User.findByPk(id);
        await user.setAdress(null);
        return res.status(200).json(user);
    }catch(err){
        return res.status(500).json({err});
    }
};

//usuário deleta comentário

const userDeleteComment = async(req, res) => {
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        Comment.destroy({where: {id: req.body.CommentId}}) 
        return res.status(200).json('Comentário deletado com sucesso!');
    }catch(err){
        return res.status(500).json({err});
    }
};

//usuário deleta produto

const userDeleteProduct = async(req, res) => {
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        Product.destroy({where: {id: req.body.ProductId}}) 
        return res.status(200).json('Produto deletado com sucesso!');
    }catch(err){
        return res.status(500).json({err});
    }
};


module.exports={
    create,
    index,
    show,
    update,
    destroy,
    addRelationAdress,
    removeRelationAdress,
    userDeleteComment,    
    userDeleteProduct,
};
