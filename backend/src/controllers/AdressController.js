const { validationResult } = require('express-validator');
const {response} = require('express');
const Adress = require('../models/Adress');

//cria um endereço
const create = async(req,res)=>{
    try{
        const adress = await Adress.create(req.body);
        return res.status(201).json({message: "Endereço cadastrado com sucesso!", adress: adress});
    }catch(err){
        res.status(500).json({error: err});
    }
};

//mostra os endereços
const index = async(req,res)=>{
    try{
        const adresses = await Adress.findAll();
        return res.status(200).json({adresses});
    }catch(err){
        res.status(500).json({err});
    }
};

//mostra um endereço
const show = async(req,res)=>{
    const{id} = req.params;
    try{
        const adress = await Adress.findByPk(id);
        return res.status(200).json({adress});
    }catch(err){
        res.status(500).json({err});
    }
};

//edita um endereço
const update = async(req,res)=>{
    const{id} = req.params;
    try{
        const [updated] = await Adress.update (req.body, {where: {id: id}});
        if(updated){
            const adress = await Adress.findByPk(id);
            return res.status(200).send(adress);
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Endereço não encontrado");
    }
};

//deleta um endereço
const destroy = async(req,res)=>{
    const {id} = req.params;
    try{
        const deleted = await Adress.destroy({where: {id: id}});
        if(deleted){
            return res.status(200).json("Endereço deletado com sucesso.");
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Endereço não encontrado.");
    }
};

module.exports={
    create,
    index,
    show,
    update,
    destroy
};
