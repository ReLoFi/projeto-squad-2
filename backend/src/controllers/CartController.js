const User = require('../models/User');
const Product = require('../models/Product');

//adiciona produto no carrinho

const addProductInCart = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        const bought = await Product.findByPk(req.body.ProductId);
        await user.addBuyer(bought);
        return res.status(200).json('Produto adicionado ao carrinho!');
    }catch(err){
        return res.status(500).json({err});
    }
};

//remove produto do carrinho

const removeProductFromCart = async(req,res) => {
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const product = await Product.findByPk(req.body.ProductId);
        await user.removeBuyer(product);
        return res.status(200).json('Produto removido do carrinho!');
    }catch(err){
        return res.status(500).json({err});
    }
};

//lista os produtos que estão no carrinho

const listProductsInCart = async(req, res) => {
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const list = await user.getBuyer();
        return res.status(200).json({ list });
    }
    catch(err) {
        return res.status(500).json({err});
    }
};

module.exports ={
    addProductInCart,
    removeProductFromCart,
    listProductsInCart
}