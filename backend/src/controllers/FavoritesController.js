const User = require('../models/User');
const Product = require('../models/Product');

//adiciona aos favoritos

const addProductsInFavorites = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        const product = await Product.findByPk(req.body.ProductId);
        await user.addFavoriting(product);
        return res.status(200).json('Produto adicionado aos favoritos!');
    }catch(err){
        return res.status(500).json(err+'');
    }
};

//remove dos favoritos

const removeProductfromFavorites = async(req,res) => {
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const product = await Product.findByPk(req.body.ProductId);
        await user.removeFavoriting(product);
        return res.status(200).json('Produto removido dos favoritos!');
    }catch(err){
        return res.status(500).json({err});
    }
};

//lista os produtos que estão nos favoritos

const listFavorites = async(req, res) => {
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const list = await user.getFavoriting();
        return res.status(200).json({ list });
    }
    catch(err) {
        return res.status(500).json(err+'');
    }
};

module.exports ={
    addProductsInFavorites,
    removeProductfromFavorites,
    listFavorites
}